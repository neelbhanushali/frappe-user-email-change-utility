# Author: Neel Bhanushali <neal.bhanushali@gmail.com>

def change_user_emails(csv_path):
    with open(csv_path) as csv_file:
        for row in csv_file.readlines():
            if row != '\n':
                row_list = row.split(',')
                old_email = row_list[0].strip()
                new_email = row_list[1].strip()
                print('updating {} to {}'.format(old_email, new_email))
                user_email_change(old_email, new_email)

def user_email_change(old_email, new_email):
    import frappe
    update_dict = {'old_email': old_email, 'new_email': new_email, 'old_email_wildcard': '%{}%'.format(old_email)}
    config = {'db_name': frappe.get_conf().get('db_name')}

    # change in User doctype
    frappe.db.sql('update `tabUser` set name=%(new_email)s, email=%(new_email)s where name=%(old_email)s;', update_dict, debug=1)
    frappe.db.sql('update `__Auth` set name=%(new_email)s where name=%(old_email)s and fieldname="password";', update_dict, debug=1)
    # ===

    # change for Roles and Permission
    frappe.db.sql('update `tabHas Role` set parent=name=%(new_email)s where parent=%(old_email)s;', update_dict, debug=1)
    frappe.db.sql('update `tabUser Permission` set user=name=%(new_email)s where user=%(old_email)s;', update_dict, debug=1)
    # ===

    # change owner and modified_by in doctypes
    tableslist = frappe.db.sql("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE 'tab%%' and TABLE_SCHEMA=%(db_name)s;", config, debug=1)
    for i in tableslist:
        if i[0] not in ['tabSingles', 'tabSeries', 'tabSessions']:
            frappe.db.sql("update `{}` set owner=%(new_email)s where owner=%(old_email)s;".format(i[0]), update_dict, debug=1)
            frappe.db.sql("update `{}` set modified_by=%(new_email)s where modified_by=%(old_email)s;".format(i[0]), update_dict, debug=1)
    # for singles
    frappe.db.sql("update `tabSingles` set value=%(new_email)s where value=%(old_email)s and field='owner';", update_dict, debug=1)
    frappe.db.sql("update `tabSingles` set value=%(new_email)s where value=%(old_email)s and field='modified_by';", update_dict, debug=1)
    # ===

    # change in doctypes where User Link is present
    tables = frappe.db.sql('select parent, fieldname from `tabDocField` where fieldtype = "Link" and options="User"')
    singles = [i[0] for i in frappe.db.sql('select name from `tabDocType` where issingle = 1;')]
    for i in tables:
        if i[0] in singles:
            frappe.db.sql('update `tabSingles` set value = %(new_email)s where doctype = "{parent}" and field="{fieldname}" and value=%(old_email)s'.format(parent=i[0], fieldname=i[1]), update_dict, debug=1)
        else:
            frappe.db.sql('update `tab{parent}` set {fieldname}=%(new_email)s where {fieldname}=%(old_email)s;'.format(parent=i[0], fieldname= i[1]), update_dict, debug=1)
    # ===

    # changes in _user_tags, _comments, _liked_by, _assign, _seen in all tables
    # sauce (get tables having particular column): https://stackoverflow.com/a/193860/9403680
    # sauce (update query with replace): https://stackoverflow.com/a/14586441/9403680
    # ---
    # for _user_tags
    tables = frappe.db.sql('SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = "_user_tags" AND TABLE_SCHEMA=%(db_name)s;', config)
    for i in tables:
        frappe.db.sql("UPDATE `{}` SET _user_tags = REPLACE(_user_tags, %(old_email)s, %(new_email)s) WHERE _user_tags LIKE %(old_email_wildcard)s".format(i[0]), update_dict, debug=1)

    # for _comments
    tables = frappe.db.sql('SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = "_comments" AND TABLE_SCHEMA=%(db_name)s;', config)
    for i in tables:
        frappe.db.sql("UPDATE `{}` SET _comments = REPLACE(_comments, %(old_email)s, %(new_email)s) WHERE _comments LIKE %(old_email_wildcard)s".format(i[0]), update_dict, debug=1)

    # for _liked_by
    tables = frappe.db.sql('SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = "_liked_by" AND TABLE_SCHEMA=%(db_name)s;', config)
    for i in tables:
        frappe.db.sql("UPDATE `{}` SET _liked_by = REPLACE(_liked_by, %(old_email)s, %(new_email)s) WHERE _liked_by LIKE %(old_email_wildcard)s".format(i[0]), update_dict, debug=1)

    # for _assign
    tables = frappe.db.sql('SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = "_assign" AND TABLE_SCHEMA=%(db_name)s;', config)
    for i in tables:
        frappe.db.sql("UPDATE `{}` SET _assign = REPLACE(_assign, %(old_email)s, %(new_email)s) WHERE _assign LIKE %(old_email_wildcard)s".format(i[0]), update_dict, debug=1)

    # for _seen
    tables = frappe.db.sql('SELECT DISTINCT TABLE_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = "_seen" AND TABLE_SCHEMA=%(db_name)s;', config)
    for i in tables:
        frappe.db.sql("UPDATE `{}` SET _seen = REPLACE(_seen, %(old_email)s, %(new_email)s) WHERE _seen LIKE %(old_email_wildcard)s".format(i[0]), update_dict, debug=1)
    # ===
